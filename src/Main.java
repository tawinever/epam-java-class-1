public class Main {
    public static void main(String[] args) {
        Gift gift = new Gift();
        gift.addCandy(new Candy(45, 1.5, 5));
        gift.addCandy(new Candy(35, 2.5, 10));

        gift.addCandy(new Candy(45, 1.5, 5));

        gift.getTotalWeight();
        gift.getPrice();

        gift.getSortedCandies(SortingParameter.PRICE);
        gift.getSortedCandies(SortingParameter.SUGAR_WEIGHT);
        gift.getSortedCandies(SortingParameter.TOTAL_WEIGHT);

        gift.findCandyBySugarWeight(9, 12);

        gift.findCandyBySugarWeight(11, 13);
    }
}
