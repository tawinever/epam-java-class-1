import java.util.Objects;

public class Candy {
    private double totalWeight;
    private double price;
    private double sugarWeight;

    public Candy(double totalWeight, double price, double sugarWeight) {
        this.totalWeight = totalWeight;
        this.price = price;
        this.sugarWeight = sugarWeight;
    }

    public double getSugarWeight() {
        return sugarWeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candy candy = (Candy) o;
        return Double.compare(candy.totalWeight, totalWeight) == 0 &&
                Double.compare(candy.price, price) == 0 &&
                Double.compare(candy.sugarWeight, sugarWeight) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalWeight, price, sugarWeight);
    }

    @Override
    public String toString() {
        return "Candy{" +
                "totalWeight=" + totalWeight +
                ", price=" + price +
                ", sugarWeight=" + sugarWeight +
                '}';
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public double getPrice() {
        return price;
    }
}
