public enum SortingParameter {
    TOTAL_WEIGHT,
    SUGAR_WEIGHT,
    PRICE
}
