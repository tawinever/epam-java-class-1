import java.rmi.UnexpectedException;
import java.util.*;

public class Gift {
    private Set<Candy> candySet = new HashSet<>();

    public void addCandy(Candy candy) {
        if(candySet.add(candy)) {
            System.out.println("Added new candy");
        } else {
            System.out.println("Couldn't add new candy. The candy already exists in the gift.");
        }
    }

    public List<Candy> getSortedCandies(SortingParameter sortingParameter) {
        List<Candy> candiesSorted = new ArrayList<>(candySet);
        Collections.sort(candiesSorted, new Comparator<Candy>() {
            @Override
            public int compare(Candy o1, Candy o2) {
                switch(sortingParameter) {
                    case PRICE:
                        return Double.compare(o1.getPrice(), o2.getPrice());
                    case TOTAL_WEIGHT:
                        return Double.compare(o1.getTotalWeight(), o2.getTotalWeight());
                    case SUGAR_WEIGHT:
                        return Double.compare(o1.getSugarWeight(), o2.getSugarWeight());
                    default:
                        throw new IllegalArgumentException();
                }
            }
        });
        System.out.println(String.format("Candies were sorted by %s", sortingParameter.name()));
        System.out.println(candiesSorted);
        return candiesSorted;
    }

    public double getPrice() {
        double price = candySet.stream().mapToDouble(Candy::getPrice).sum();
        System.out.println(String.format("Total price is %.2f", price));
        return price;
    }

    public double getTotalWeight() {
        double totalWeight =  candySet.stream().mapToDouble(Candy::getTotalWeight).sum();
        System.out.println(String.format("Total Weight is %.2f", totalWeight));
        return totalWeight;
    }

    public Optional<Candy> findCandyBySugarWeight(double minimumWeight, double maximumWeight) {
        Optional<Candy> candyOptional =  candySet.stream()
                .filter(o -> minimumWeight <= o.getSugarWeight())
                .filter(o -> maximumWeight >= o.getSugarWeight())
                .findAny();

        if (candyOptional.isPresent()) {
            System.out.println("Candy was found:");
            System.out.println(candyOptional.get());
        } else {
            System.out.println("Couldn't find any candy in the current interval");
        }
        return candyOptional;
    }

}
